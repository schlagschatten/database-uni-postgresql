import React, {Fragment, useEffect} from 'react'
import '../../App.css'
import ClientItem from "./ClientItem";
import InputClient from "./InputClient";

let actions = require("../../app/actions.jsx");
let connect = require("react-redux").connect;

const ClientsList = (props, header) => {

    // create a product

    const getClients = async () => {
        try {

            const response = await fetch(`http://localhost:5000/clients`);
            const jsonData = await response.json();

            jsonData.forEach((client) => props.addClient(client));
        } catch (err) {
            console.error(err.message);
        }
    };

    useEffect(() => {
        getClients().then(clients =>
            clients);

    }, []);


    const loadedClients = () => {
        return [...props.clients][0][1];
    }

    // delete a product

    const deleteClient = async (client) => {
        try {
            const response = await fetch(`http://localhost:5000/clients/${client.id}`, {
                method: "DELETE"
            })

            console.log(response);
            props.deleteClient(client);
        } catch (err) {
            console.error(err.message)
        }
    }

    // update a product

    const updateClient = async (client) => {
        try {
            const response = await fetch(`http://localhost:5000/clients/${client.id}`, {
                method: "PUT",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify({client: client})
            })

            props.editClient(client);
        } catch (err) {
            console.error(err.message);
        }
    }

    return (

        <Fragment>
            <InputClient header={"Add hot-dog"}/>
            {" "}
            <div className="small_header">All clients</div>

            <div className='products_content'>

                {loadedClients().map(client => (
                    <div className='products_block'>
                        <ClientItem client={client} onSubmit={updateClient} onSubmitDelete={deleteClient}/>
                    </div>
                ))}

            </div>
        </Fragment>
    )
};

function mapStateToProps(state) {
    return {
        clients: state.clients
    };
}

export default connect(mapStateToProps, actions)(ClientsList);

