import React from 'react';
import EditClient from "./EditClient";
import ClientShow from "./ClientShow";

const ClientItem = ({client, onSubmitDelete, onSubmit}) => {
    const [showResults, setShowResults] = React.useState(false)
    const onEdit = () => {
        setShowResults(true)
        console.log(showResults);
    }

   function onSubmit2(value){
        if(value.action === 'delete') {
            onDelete(value)
        }
        else{
            console.log("update1")
            onSubmitUpgrade(value)
        }

    }

    function onSubmitUpgrade(e){
        console.log("update1")
        onSubmit(e);
        setShowResults(false);
    }

    function onDelete(e){
        onSubmitDelete(e)
        setShowResults(false);
    }
    return (
        <div>
            {showResults}

            { showResults ? null : <ClientShow client={client} onEdit={onEdit}/> }


            { showResults ? <EditClient form={`client_${client.id}`} initialValues={client} onSubmit={onSubmit2} /> : null }

        </div>
    );
};

export default ClientItem;