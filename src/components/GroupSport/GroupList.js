import React, {Fragment, useState} from 'react'
import '../../App.css'



class GroupList extends React.Component {
state = {groups:[]}
    //const [groups, setGroups] = useState([]);

    componentDidMount(){
    console.log('on mount');
        fetch(`http://localhost:5000/group_sport`, {
            method: 'GET',
        })
            .then((response)=>{return response.json();
            })
            .then((json)=>{
                //setGroups(json);
                this.setState({groups:json});//.state.groups = json;
            });
    }


    render() {
        return (
            <Fragment>
                {" "}
                <div className="small_header">All groups</div>

                <div className="main_block">
                    <div className='block_tittle'>
                        <p className="block_tittle_name">Name</p>
                        <p className="block_tittle_name">Price</p>
                        <p className="block_tittle_name">Limit Child</p>
                        <p className="block_tittle_name">Sport Organization</p>
                    </div>

                    <div className='products_content'>
                        {this.state.groups.map(group => (
                            <div>
                                <p>{group.name_group}</p>
                                <p>{group.price}</p>
                                <p>{group.limit_child}</p>
                                <p>{group.sport_organization}</p>

                            </div>
                        ))}

                    </div>
                </div>

            </Fragment>
        )
    }
};

export default GroupList;

