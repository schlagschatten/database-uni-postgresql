import React, {Fragment} from 'react';
import GroupList from "./GroupSport/GroupList";
import SportOrganizationList from "./SportOrganization/SportOrganizationList";
import TrainerList from "./Trainer/TrainerList";


const Main = () => {

    return (
            <Fragment>
                <GroupList/>
                <SportOrganizationList/>
                <TrainerList/>
            </Fragment>

    );
};

export default Main


