import React, {Fragment, useState} from 'react'
import '../../App.css'

class RequestSecond extends React.Component {
    state = {clients:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/request_second`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({clients: json});
            });
    }

    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">Учні, які проживають в місті Davis та ходять в одну організацію «Harmonic Inc.»</div>

                <div className='products_content'>
                    {this.state.clients.map(client => (
                        <div className="products_block_request">
                            <p>{client.full_name}</p>
                        </div>
                    ))}

                </div>
            </Fragment>
        );
    }

};

export default RequestSecond;