import React from 'react';
import RequestFirst from "./RequestFirst";
import RequestSecond from "./RequestSecond";
import RequestThird from "./RequestThird";
import RequestFourth from "./RequestFourth";
import RequestFifth from "./RequestFifth"

const Request = () => {
    return (
        <div>
            <RequestFirst/>
            <RequestSecond/>
            <RequestThird/>
            <RequestFourth/>
            <RequestFifth/>
        </div>
    );
};

export default Request;