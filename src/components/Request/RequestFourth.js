import React, {Fragment, useState} from 'react'
import '../../App.css'

class RequestFourth extends React.Component {
    state = {clients:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/request_fourth`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({clients: json});
            });
    }

    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">Загальна сума сплат клієнтів</div>

                <div className='products_content'>
                    {this.state.clients.map(client => (
                        <div className="products_block_request">
                            <p>{client.full_name}</p>
                            <p>{client.sumforgroup}$</p>
                        </div>
                    ))}

                </div>
            </Fragment>
        );
    }

};

export default RequestFourth;