import React, {Fragment, useState} from 'react'
import '../../App.css'

class RequestFifth extends React.Component {
    state = {trainers:[]}

    componentDidMount() {
        fetch(`http://localhost:5000/request_fifth`, {
            method: 'GET',
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({trainers: json});
            });
    }

    render(){
        return (
            <Fragment>
                {" "}
                <div className="small_header">Тренери та кількість занять, яку вони провели</div>

                <div className='products_content'>
                    {this.state.trainers.map(trainer => (
                        <div className="products_block_request">
                            <p>{trainer.full_name}</p>
                            <p>{trainer.count}</p>
                        </div>
                    ))}

                </div>
            </Fragment>
        );
    }

};

export default RequestFifth;