let addClient = function (client) {
    return {
        type: "ADD_CLIENT",
        client
    }
};
let editClient = function (client) {
    return {
        type: "EDIT_CLIENT",
        client
    }
};

let deleteClient = function (client) {
    return {
        type: "DELETE_CLIENT",
        client
    }
};

module.exports = {addClient, editClient, deleteClient};