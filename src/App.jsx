import React, {useState} from 'react';
import {Link, BrowserRouter as Router, Route} from "react-router-dom";
import Main from "./components/Main";
import './App.css';
import Menu from "./components/Menu/Menu";
import GroupList from "./components/GroupSport/GroupList";
import SportOrganizationList from "./components/SportOrganization/SportOrganizationList";
import TrainerList from "./components/Trainer/TrainerList";
import Request from "./components/Request/Request";
import Profile from "./components/Profile"
import Form from "./components/Form"

function App() {

    const [menuActive, setMenuActive] = useState(false)

    const items = [
        {value: "Group Sport", href: '/group_sport'},
        {value: "Sport Organization", href: '/sport_organization'},
        {value: "Trainer", href: '/trainer'},
        {value: "Request", href: '/request'},
        {value: "Profile", href: '/profile'},
        {value: "Login", href: '/login'}
    ]

    return (

        <section className="App">
            <div className='app'>
                <nav>
                    <div className="main-menu-btn" onClick={() => setMenuActive(!menuActive)}>
                        <span></span>
                    </div>
                </nav>
                <main>
                    <div>
                        {<Menu active={menuActive} setActive={setMenuActive} header={"Menu"} items={items}/>}
                    </div>

                    <div>
                        <Router>
                            <Link to="/"></Link>
                            <Link to="/group_sport"></Link>
                            <Link to="/sport_organization"></Link>
                            <Link to="/trainer"></Link>
                            <Link to="/request"></Link>
                            <Link to="/profile"></Link>
                            <Link to="/login"></Link>
                            <Route exact path="/" component={Main} />
                            <Route exact path="/group_sport" component={GroupList} />
                            <Route exact path="/trainer" component={TrainerList}/>
                            <Route exact path="/sport_organization" component={SportOrganizationList} />
                            <Route exact path="/request" component={Request}/>
                            <Route exact path="/profile" component={Profile}/>
                            <Route exact path="/login" component={Form} />

                        </Router>
                    </div>
                </main>
            </div>
        </section>
    );
}

export default App;
