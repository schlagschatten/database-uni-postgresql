const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db")

app.use(cors())
app.use(express.json())


//create a client

app.post("/clients", async (req, res) => {
        try {
            const clientt = await pool.connect()
            const {full_name, date_of_birth, town, street, name_one_parent, number_telephone_of_parent} = req.body;


            const newClientInfo = await clientt.query(
                "INSERT INTO more_information (town, street, name_one_parent, number_telephone_of_parent) VALUES($1, $2, $3, $4) RETURNING * ",
                [town, street, name_one_parent, number_telephone_of_parent]
            );
            console.log(newClientInfo)

            const newClient = await clientt.query(
                "INSERT INTO client (full_name, date_of_birth, more_information) VALUES($1, $2, $3) RETURNING * ",
                [full_name, date_of_birth, newClientInfo.rows[0].id]
            );

            console.log(newClientInfo)

            clientt.release(newClientInfo);
            await res.json(newClient.rows[0])
        } catch (err) {
            console.log(err.message)
        }
    }
)

//get all clients

app.get("/clients", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT client.id, client.full_name, client.date_of_birth, mi.town, mi.street, mi.name_one_parent, mi.number_telephone_of_parent FROM client INNER JOIN more_information mi ON client.more_information=mi.id ").then((allClients) => {

            clientt.release();
            res.json(allClients.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

// //get a clients

app.get("/clients/:id", async (req, res) => {
    try {
        const {id} = req.params;
        const clientt = await pool.connect()
        const client = await clientt.query("SELECT * FROM client WHERE id = $1",
            [id]
        )
        clientt.release();
        await res.json(client.rows[0])
    } catch (err) {
        console.log(req.params)
    }

})

//update product
app.put("/clients/:id", async (req, res) => {
    try {
        console.log("111")
        const clientt = await pool.connect()
        console.log("222")
        const {id, full_name, date_of_birth, town, street, name_one_parent, number_telephone_of_parent} = req.body.client;
        console.log("333")
        const updateClient = await clientt.query("UPDATE client SET full_name=$2, date_of_birth=$3 WHERE id=$1 RETURNING*",
            [id, full_name, date_of_birth]
        );

        const updateMoreInfo = await clientt.query("UPDATE more_information SET town=$2, street=$3, name_one_parent=$4, number_telephone_of_parent=$5 WHERE id=$1",
            [updateClient.rows[0].more_information, town, street, name_one_parent, number_telephone_of_parent]
        );

        console.log(updateClient)
        console.log(updateClient.rows[0].more_information)

        clientt.release();
        await res.json(updateClient)

    } catch (err) {
        console.error(err.message);
        await res.json(err);
    }
})


//delete a clients

app.delete("/clients/:id", async (req, res) => {
    try {
        const clientt = await pool.connect()
        const id = req.params.id;
        const deleteClient = await clientt.query("DELETE FROM client WHERE id = $1",
            [id]
        );
        clientt.release();
        await res.json(deleteClient)
    } catch (err) {
        console.error(err.message);
    }
})


//get all groups

app.get("/group_sport", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT * FROM group_sport").then((allGroups) => {

            clientt.release();
            res.json(allGroups.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.get("/sport_organization", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT * FROM sport_organization").then((allClients) => {

            clientt.release();
            res.json(allClients.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.get("/trainer", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT * FROM trainer").then((allClients) => {

            clientt.release();
            res.json(allClients.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        res.json(err.message);
    }
})

// REQUEST

app.get("/request_first", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT client.full_name FROM client INNER JOIN client_groups cg ON client.id = cg.id_client INNER JOIN group_sport gs ON cg.id_groups = gs.id INNER JOIN sport_organization so ON gs.sport_organization = so.id WHERE so.type_sport = 'Chess'").then((allGroups) => {

            clientt.release();
            res.json(allGroups.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.get("/request_second", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT client.full_name FROM client INNER JOIN client_groups cg ON client.id = cg.id_client INNER JOIN more_information mi ON client.more_information = mi.id INNER JOIN group_sport gs ON cg.id_groups = gs.id INNER JOIN sport_organization so ON gs.sport_organization = so.id WHERE mi.town='Davis' AND so.name_organization = 'Harmonic Inc.'").then((allGroups) => {

            clientt.release();
            res.json(allGroups.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.get("/request_third", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT client.full_name AS FULL_NAME,  (DATE_PART('year', CURRENT_DATE) - DATE_PART('year', client.date_of_birth::date)) AS Years FROM client WHERE (DATE_PART('year', CURRENT_DATE) - DATE_PART('year', client.date_of_birth::date))< 16").then((allGroups) => {

            clientt.release();
            res.json(allGroups.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.get("/request_fourth", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT client.full_name, SUM(gs.price)*COUNT(gs.id) AS SUMFORGROUP FROM client INNER JOIN client_groups cg ON client.id = cg.id_client INNER JOIN group_sport gs ON cg.id_groups = gs.id GROUP BY client.full_name").then((allGroups) => {

            clientt.release();
            res.json(allGroups.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.get("/request_fifth", async (req, res) => {
    try {
        const clientt = await pool.connect()
        clientt.query("SELECT trainer.full_name, COUNT(gs.id) FROM trainer INNER JOIN group_sport gs ON trainer.id = gs.trainer GROUP BY trainer.full_name").then((allGroups) => {

            clientt.release();
            res.json(allGroups.rows)
        }).catch((err) => {
            console.error(err.message)
            res.json(err.message);
        });

    } catch (err) {
        console.error(err.message)
        await res.json(err.message);
    }
})

app.listen(5000, () => {
    console.log("server port 5000")
})